# screen-runner-easter-egg

A JavaScript function that moves an image across the screen.  Original implementation was a caped Mario.  Intended use is for Easter Eggs.

### To-do:
+ Right now frame() just runs until position is > 3500, but I'm only guessing that this will be far enough off to necessarily be off the screen.  It would be better if I could find a way to actually test for screen visibility.  Probably by querying the media width.
+ This is a small non-issue, but if you scroll the page the runner stays in the same position on the screen.  I think it would look better if they stayed on the same vertical position relative to page, not to screen.
