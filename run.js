function runningEasterEgg(uri) {
    let runnerId = "anything-unique-should-do-the-trick";
    if (document.getElementById(runnerId)) {
        console.log("don't create a runner if there's already a runner instance");
        return;
    }
    // create a new element, set attributes
    let easterEgg = document.createElement('IMG');
    easterEgg.setAttribute('id', runnerId);
    let startPosition = -50;
    // add CSS to make sure it runs across a visible part of the screen
    easterEgg.style.position = 'fixed';
    easterEgg.style.top = '50%';
    // style.left doesn't seem to work here in all contexts (namely bootstrap 4)
    easterEgg.style.setProperty("left", startPosition + "px", "important");
    easterEgg.style.zIndex = '999';
    easterEgg.src = uri;
    let elem = document.body.appendChild(easterEgg);
    // semantic code is better, once we increment this it's not really "startPosition" anymore
    let position = startPosition;
    let intervalId = setInterval(frame, 10);

    function frame() {
        if (position >= 3500) {
            clearInterval(intervalId);
            elem.parentNode.removeChild(elem); // this is an example of lexial scoping
        } else {
            position += 15;
            elem.style.setProperty("left", position + "px", "important");
        }
    }
}
